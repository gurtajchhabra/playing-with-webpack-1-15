playing-with-webpack-1-15

# Repo Forked from https://github.com/sharynneazhar/react-jeopardy
## Updated Webpack version
## Added sass-loader and font-face support
## Added webpack plugins for minifying
## Added watch mode and changed build command to "npm start". Live updates are supported now.

- To look into webpack-dev-server
- See how upgrading to webpack 4.43 affects the repo
- Additionally, use this boilerplate to try a webpack React app
- https://medium.com/a-beginners-guide-for-webpack-2 
