var path = require("path");
var webpack = require("webpack");

module.exports = {
  entry: "./js/app.js",
  output: {
    path: path.resolve(__dirname, "build"),
    filename: "app.bundle.js",
  },
  watch: true,
  module: {
    loaders: [
      {
        test: /\.js$/,
        loader: "babel-loader",
        query: {
          presets: ["es2015", "react"],
        },
      },
      {
        test: /\.js$/,
        loader: "babel-loader",
        query: {
          presets: ["es2015", "react"],
        },
      },
      {
        test: /\.(s*)css$/,
        loader: "style-loader!css-loader!sass-loader",
      }
    ],
  },
  stats: {
    colors: true,
  },
  devtool: "source-map",
};
